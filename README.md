# React Seeder #

React seeder with PostCSS, Stylus, Babel ES2016, Stage-1 Preset, and polyfills.

### Dependencies ###
* Webpack v3+
* stylus v0.5+
* babel v6+
* cssnano v3+
* react v15+
* react-dom v15+
* react-redux v5+
* redux v3.7+
* react-router v4.1+

#### Current Version ####
` 1.0.0 `

### How to set up? ###
`
npm install
`

### NPM Scripts ###

* `npm start` - start development server on port 4000
* `npm run build` - building dist folder project

### Production Mode ###
* edit `env.js` prodEnvironment to `true` to start minifying files and gzip compression