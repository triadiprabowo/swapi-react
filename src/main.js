// core dependencies
import React from "react";
import ReactDOM from "react-dom";

import AppRouter from './app/router';

// define selector
const app = document.getElementById('app-root');

// define global api path
global['API_PATH'] = 'https://swapi.co/api';

// render react to app selector
ReactDOM.render(React.createElement(AppRouter), app);