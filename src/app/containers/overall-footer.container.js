import React from 'react';

export default class OverallFooter extends React.Component {
	render() {
		return (
			<footer class="footer__wrapper">
				<div class="footer__container">
					<p>&copy; 2017 Triadi Prabowo, for <a href="http://mockapps.site" target="_blank">Mockapps.site</a> open source project</p>
				</div>
			</footer>
		);
	}
}