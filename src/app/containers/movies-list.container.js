import React from 'react';

export default class MoviesList extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const movies = this.props.movies;

		if(movies.fetching) {
			return <h1>Loading movies...</h1>;
		}
		else {
			return (
				<section class="card__wrapper card--round card--pos-4">
					{movies.data.map(movie => 
						<div class="card__inner" key={movie.episode_id}>
							<h1>{movie.title}</h1>
							<p>Opening Crawl: {movie.opening_crawl}</p>
						</div>
					)}
				</section>
			);
		}
	}
}