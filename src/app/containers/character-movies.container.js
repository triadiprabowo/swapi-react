import React from 'react';
import * as $ from 'jquery';

// import action
import { MovieAction } from '../actions/movies.action';

export default class CharMovieList extends React.Component {
	constructor(props) {
		super(props);

		this.moviesAction = new MovieAction();
	}

	componentWillMount() {
		const characterData = this.props.character.data;
		this.props.dispatcher(this.moviesAction.getByURL(characterData.films));
	}

	changeSelected(data) {		
		this.props.dispatcher({type: 'SET_MOVIE_SELECTED', payload: data});

		$('body').animate({
			scrollTop: $('#selected-movie-container').offset().top
		}, 500);
	}

	render() {
		const characterData = this.props.character.data;
		const characterName = characterData.name.match(/\S+/g) || [];
		const lastName = characterName[characterName.length-1];

		if(!this.props.movies.fetched) {			
			return (
				<section class="block__section">
					<h1 class="section__title">{lastName}'s Movies</h1>

					<div class="grid__loader align--left space--minimum">
						<h1>Loading related movies...</h1>
					</div>
				</section>
			);
		}
		else {
			return (
				<section class="block__section">
					<h1 class="section__title">{lastName}'s Movies</h1>

					<section class="card__wrapper card--round card--pos-4">					
						{this.props.movies.data.map(movie =>
							<div class="card__inner has--header has--cursor" key={movie.episode_id} 
								onClick={this.changeSelected.bind(this, movie)}>

								<div class="card__header">
									<img src="assets/images/play.png" />
								</div>

								<div class="card__title">
									{movie.title}
								</div>

								<div class="card__space">
									<p>Director:</p>
									<p>{movie.director}</p>
								</div>

								<div class="card__space">
									<p>Release:</p>
									<p>{movie.release_date}</p>
								</div>
							</div>
						)}
					</section>
				</section>
			);
		}
	}
}