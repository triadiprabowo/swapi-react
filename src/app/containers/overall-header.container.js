import React from 'react';

export default class OverallHeader extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<header class="header__wrapper">
				<div class="header__container">
					<div class="logo">
						<h1>SWAPI React</h1>
						<p>Star Wars Library for Fans Out There!</p>
					</div>
				</div>
			</header>
		)
	}
}