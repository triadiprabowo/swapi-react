import React from 'react';

export default class CharDetail extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<section class="block__section">
					<h1 class="section__title has--border-left">{this.props.character.name}</h1>
				</section>

				<section class="block__space bg--black">
					<div class="block__flex">
						<div class="block__flex__inner flex--grow-y has--background"  style={{backgroundImage: "url('assets/images/user.png')"}}></div>

						<div class="block__flex__inner flex--grow-y">
							<p>Height: {this.props.character.height}cm</p>
							<p>Hair Color: {this.props.character.hair_color}</p>
							<p>Skin Color: {this.props.character.skin_color}</p>
							<p>Mass: {this.props.character.mass}</p>
							<p>Birth Year: {this.props.character.birth_year}</p>
							<p>Gender: {this.props.character.gender}</p>
						</div>
					</div>
				</section>
			</div>
		);
	}
}