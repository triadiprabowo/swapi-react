import React from 'react';

export default class CharMovieSelected extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		if(this.props.movies.fetched) {
			return (
				<section class="block__section" id="selected-movie-container">
					<h1 class="section__title clr--red">Related Movie</h1>

					<section class="card__wrapper card--pos-1">
						<div class="card__inner has--header header--black header--relative">
							<div class="card__header">
								<h1>{this.props.movies.selected.title}</h1>
								<table class="table no--border text--bold">
									<tbody>
										<tr>
											<td>Director:</td>
											<td>{this.props.movies.selected.director}</td>
										</tr>

										<tr>
											<td>Producer:</td>
											<td>{this.props.movies.selected.producer}</td>
										</tr>

										<tr>
											<td>Release Date:</td>
											<td>{this.props.movies.selected.release_date}</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="card__space">
								<p>{this.props.movies.selected.opening_crawl}</p>
							</div>
						</div>
					</section>
				</section>
			);
		}
		else {
			return (
				<section class="block__section">
					<h1 class="section__title clr--red">Related Movie</h1>
				</section>
			);
		}			
	}
}