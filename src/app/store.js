import { applyMiddleware, createStore } from "redux";
import { createLogger as logger } from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import { prodEnvironment } from '../../env';

// import reducer index
import Reducers from "./reducers";

let Middleware;

if(!prodEnvironment) {
	Middleware = applyMiddleware(promise(), thunk, logger());
	
}
else {
	Middleware = applyMiddleware(promise(), thunk);
}

export default createStore(Reducers, Middleware);