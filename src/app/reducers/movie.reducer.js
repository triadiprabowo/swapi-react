// create movie reducer

export default function reducer(state={
	data: null,
	selected: null,
	fetched: false,
	fetching: false
}, action) {
	switch(action.type) {
		case 'GET_MOVIES_LOADING': {
			return { ...state, fetching: true, fetched: false };
		}

		case 'GET_MOVIES_OK': {
			return { data: action.payload, fetched: true, fetching: false, selected: action.payload[0] };
		}

		case 'GET_MOVIES_ERR': {
			return { ...state, fetched: false, fetching: false };
		}

		case 'SET_MOVIE_SELECTED': {
			return { ...state, fetched: true, selected: action.payload };
		}
	}

	return state;
}