// import core module
import { combineReducers } from "redux"

// import reducers
import CharacterReducer from './character.reducer';
import MovieReducer from './movie.reducer';
import HometownReducer from './hometown.reducer';

export default combineReducers({
	CharacterReducer,
	MovieReducer,
	HometownReducer
});
