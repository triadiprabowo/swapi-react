// create character reducer

export default function reducer(state={
	data: null,
	fetched: false,
	fetching: false
}, action) {
	switch(action.type) {
		case 'GET_HOMETOWN_LOADING': {
			return { data: null, fetching: true, fetched: false };
		}

		case 'GET_HOMETOWN_OK': {
			return { data: action.payload, fetched: true, fetching: false };
		}

		case 'GET_HOMETOWN_ERR': {
			return { ...state, fetched: 'ERR', fetching: false };
		}
	}

	return state;
}