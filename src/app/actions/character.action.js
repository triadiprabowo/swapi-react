import * as $ from 'jquery';

export class CharacterAction {
	constructor() {
		// bind class properties
		this.getCharacter = this.getCharacter.bind(this);
	}

	getCharacter(id) {
		id = id? id : '';

		return (dispatch) => {
			dispatch({type: 'GET_CHARACTER_LOADING'});

			$.get(API_PATH+'/people/'+id)
			.then(function(response) {
				dispatch({type: 'GET_CHARACTER_OK', payload: (id? response : response.results) });
			})
			.catch(function(err) {
				dispatch({type: 'GET_CHARACTER_ERR', payload: err.responseJSON});
			});
		}
	}
}