import * as $ from 'jquery';

export class HometownAction {
	constructor() {
		// binding class properties
		this.getByURL = this.getByURL.bind(this);
	}

	getByURL(url) {
		return (dispatch) => {
			dispatch({type: 'GET_HOMETOWN_LOADING'});

			$.get(url)
			.then(function(response) {
				dispatch({type: 'GET_HOMETOWN_OK', payload: response})
			})
			.catch(function(err) {
				dispatch({type: 'GET_HOMETOWN_ERR', payload: err.responseJSON})
			})
		}		
	}
}