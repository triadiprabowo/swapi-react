import * as $ from 'jquery';
import * as Q from 'q';

export class MovieAction {
	constructor() {
		// bind function
		this.getAll = this.getAll.bind(this);
		this.getByURL = this.getByURL.bind(this);
	}

	getAll() {

		return (dispatch) => {

			dispatch({ type: 'GET_MOVIES_LOADING' });

			$.get(API_PATH+'/films')
			.then(function(response) {
				dispatch({ type: 'GET_MOVIES_OK', payload: response.results });
			})
			.catch(function(err) {
				dispatch({ type: 'GET_MOVIES_ERR', payload: err.responseJSON });
			});	
		}		
	}

	getByURL(arr) {
		if(!arr && typeof arr != 'array') return console.error('Arguments movies by ID must be array and minimum 1 data');
		let qData = [];

		return (dispatch) => {
			dispatch({type: 'GET_MOVIES_LOADING' });			

			for(let i=0; i < arr.length; i++) {
				qData.push($.get(arr[i]))
			}

			Q.all(qData)
			.then(function(r) {
				dispatch({type: 'GET_MOVIES_OK', payload: r});
			})
			.catch(function(err) {
				dispatch({type: 'GET_MOVIES_ERR', payload: err});
			})
		}
	}
}