import React, { Component } from 'react';
import * as $ from 'jquery';

export default class NotFoundComponent extends Component {
	constructor() {
		super();

		this.reload = this.reload.bind(this);
	}

	componentWillMount() {
		$('body').addClass('page--not-found')
	}

	reload() {
		window.location.reload();
	}

	render() {
		return (
			<section class="page__wrapper">
				<div class="description__404">
					<h1>Error 404 - page not found.</h1>
					<p>The page you are looking for is dead by the darkness</p>
					<button class="button button--green" onClick={this.reload}>Reload</button>
				</div>
			</section>
		);
	}
}