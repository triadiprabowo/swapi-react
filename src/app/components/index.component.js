// import core modules
import React from 'react';
import { connect } from 'react-redux';

// import actions
import { CharacterAction } from '../actions/character.action';

// import containers
import OverallHeader from '../containers/overall-header.container';
import OverallFooter from '../containers/overall-footer.container';
import MoviesList from '../containers/movies-list.container';
import CharMovieList from '../containers/character-movies.container';
import CharDetail from '../containers/character-detail.container';
import CharMovieSelected from '../containers/character-movselected.container';

@connect((store) => {
	return {
		character: store['CharacterReducer'],
		movies: store['MovieReducer'],
		hometown: store['HometownReducer']
	}
})
export default class IndexComponent extends React.Component {
	constructor(props) {
		super(props);

		// inject actions
		this.characterAction = new CharacterAction();
	}

	componentWillMount() {
		this.props.dispatch(this.characterAction.getCharacter(1));
	}

	render() {
		if(!this.props.character.fetched) {
			return(
				<section class="page__wrapper">
					{/* insert overall header */}
					<OverallHeader></OverallHeader>

					<div class="grid__loader">
						<h1>Loading data...</h1>
					</div>
				</section>
			);			
		}
		else {
			return (
				<section class="page__wrapper">
					{/* insert overall header */}
					<OverallHeader></OverallHeader>

					<div class="grid__block grid--100">
						<CharDetail character={this.props.character.data}></CharDetail>
					</div>					

					<div class="grid__block grid--60">
						<CharMovieList dispatcher={this.props.dispatch} character={this.props.character} 
							movies={this.props.movies}></CharMovieList>
					</div>

					<div class="grid__block grid--40">
						<CharMovieSelected movies={this.props.movies}></CharMovieSelected>
					</div>

					<div class="clearfix"></div>

					{/* insert overall footer */}
					<OverallFooter></OverallFooter>
				</section>
			)
		}		
	}
}