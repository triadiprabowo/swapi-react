// react router
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";

// import store
import Store from './store';

// import components
import IndexComponent from './components/index.component';
import NotFoundComponent from './components/404.component';

export default class AppRouter extends React.Component {
	render() {
		return (
			<Provider store={Store}>
				<Router>
					<div>
						<Switch>
							<Route exact path="/" component={IndexComponent} />
							<Route component={NotFoundComponent} />
						</Switch>
					</div>
				</Router>
			</Provider>
		);
	}
}